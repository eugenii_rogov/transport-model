from transport.car import Car
from transport.train import Train
from oil_base import OilBase

from helpers import get_cost_by_distance

import pulp as p


def run():
    oil_bases = [OilBase(800, 950, 'Домодедово', 400, 5)]

    for oil_base in oil_bases:
        auto_cost, train_cost = get_cost_by_distance(train_distance=oil_base.train_distance,
                                                     auto_distance=oil_base.auto_distance)
        problem = p.LpProblem('transport_problem', p.LpMinimize)
        lp_cars_count = p.LpVariable('cars', lowBound=0, cat='Integer')
        lp_trains_count = p.LpVariable('trains', lowBound=0, cat='Integer')
        lp_cost = p.LpVariable('cost', lowBound=0, cat='Integer')
        lp_time = p.LpVariable('time', lowBound=0, upBound=oil_base.days_for_delivery * 24, cat='Integer')

        problem += lp_cars_count * Car.load_capacity + lp_trains_count * Train.load_capacity >= oil_base.inquiry
        problem += auto_cost * oil_base.auto_distance * lp_cars_count + \
                   train_cost * oil_base.train_distance * lp_trains_count == lp_cost
        problem.solve()

        a = p.value(lp_cars_count)
        t = p.value(lp_trains_count)
        c = p.value(lp_cost)
        time = p.value(lp_time)
        print(f"auto count: {a},"
              f"train count: {t},"
              f"cost {c},"
              f"time {time}")


run()
