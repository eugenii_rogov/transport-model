def get_cost_by_distance(train_distance: int, auto_distance: int) -> tuple:
    if auto_distance >= 2500:
        auto_cost = 2.7 + 40
    elif 1300 <= auto_distance < 2500:
        auto_cost = 2.5 + 40
    else:
        auto_cost = 3.5 + 40

    if train_distance >= 2500:
        train_cost = 1.8
    elif 1300 <= train_distance < 2500:
        train_cost = 2.5
    else:
        train_cost = 3

    return auto_cost, train_cost
