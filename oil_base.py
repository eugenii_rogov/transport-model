class OilBase:
    def __init__(self, auto_distance: int, train_distance: int, name: str, inquiry: int, days_for_delivery: int):
        """
        Инициализация объекта НБ
        :param distance: Дистанция до распределительного пункта, км
        :param name: Имя НБ
        :param inquiry: Запрос нефтепродуктов, т
        """
        self.days_for_delivery = days_for_delivery
        self.auto_distance = auto_distance
        self.train_distance = train_distance
        self.name = name
        self.inquiry = inquiry
